﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/// <summary>
/// generalize level editor for gridbased games that lets you click and place objects on and have them align to the grid
/// for now it really only supports mouse input but it would be cool to have it support controllers later
/// </summary>
public class LevelEditor : MonoBehaviour
{
    enum colType { grid, collider } //could potentially add a 3d type?

    [SerializeField]
    [Tooltip("drag in any type of block you want to be able to place")]
    List<GameObject> tiles;
    int index = 0;
    [SerializeField] colType collisionType;
    [SerializeField] bool alignToGrid = true, overWrite = true;
    [SerializeField] string placeInput, removeInput, scrollAxis;
    Event e;
    // [SerializeField] int layer; //for future use
    Vector2 mouseVector, gameVector; //the latter is the former converted into world space
    GameObject somethingThere;
    private void OnEnable()
    {
        EditorApplication.update += EditorUpdate;
    }
    private void OnDisable()
    {
        EditorApplication.update -= EditorUpdate;
    }
    private void Update()
    {
        EditorUpdate();
    }
    
    void EditorUpdate()
    {
        e = Event.current;
        UpdateVector();
        SpawnInput();
        DespawnInput();
        ScrollingInput();
    }

    void UpdateVector()
    {
        mouseVector = Input.mousePosition;
        gameVector = Camera.main.ScreenToWorldPoint(mouseVector, Camera.MonoOrStereoscopicEye.Mono);
        if (alignToGrid)
        {
            gameVector = GridManagement.RoundVectorToInts(gameVector);

        }
 
    }
    void DespawnInput()
    {
        if (e!=null && e.type == EventType.MouseDown && e.button == 1)
        {

            print("updating");
            AbleToPlace(true);
        }
    }
    void SpawnInput()
    {

        if (e != null && e.type == EventType.MouseDown && e.button == 0)
        {

            if (AbleToPlace(overWrite))
            {
                SpawnObject();
            }

        }
    }
    void ScrollingInput()
    {
        if (Input.GetAxis(scrollAxis) > 0)
        {
            index++;
            if (index >= tiles.Count)
            {
                index = 0;
            }
        }
        else if (Input.GetAxis(scrollAxis) < 0)
        {
            index--;
            if (index < 0)
            {
                index = tiles.Count - 1;
            }
        }

    }
    void SpawnObject()
    {
        Instantiate(tiles[index], gameVector, Quaternion.identity);
        //I guess this is only one line for now but I can see this getting more complicated over time
    }
    /// <summary>
    /// determines whether it's possible to place a block at mouse position, and if overwrite is on, it makes sure it's possible
    /// </summary>
    /// <returns></returns>
    bool AbleToPlace(bool canDestroy)
    {
        switch (collisionType)
        {
            case (colType.grid):
                //check if there's something already there
                somethingThere = GridManagement.getObjectFromPosition(gameVector); //Gridmanagement always rounds this to an int even if the position you're placing at wont be
                if (somethingThere)
                {
                    if (canDestroy)
                    {
                        //delete the object that is currently there
                        GridManagement.DeleteObject(somethingThere);
                    }
                    else
                    {
                        //just dont spawn anything for now
                        return false;
                    }
                }


                break;
            case (colType.collider):
                Collider2D col = Physics2D.OverlapPoint(gameVector); //this should probably have layer checks too
                if (col)
                {
                    somethingThere = col.gameObject;
                }
                if (somethingThere)
                {
                    if (canDestroy)
                    {
                        Destroy(somethingThere);
                    }
                    //else just let it get placed since if its off grid it probably doesn't matter if things overlap
                }
                break;
        }
        return true;
    }
}
