﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    Vector3 dir;
    bool moving;
    [SerializeField] bool player1; //only player 1 can do zoomb and automation
    [SerializeField] float inputThresh;
    [SerializeField] float maxZoom, minZoom, zoomSpeed;
    [SerializeField] bool partOfTheSystem, canPush;
    [SerializeField] string hInput, vInput;
    Camera camComp;
    int thisScene;
    // Use this for initialization
    void Start()
    {
        if (partOfTheSystem)
        {
            gameObject.tag = "Pushable";
            gameObject.AddComponent<Cell>();
        }
        camComp = GetComponentInChildren<Camera>();
        thisScene = gameObject.scene.buildIndex;
    }

    // Update is called once per frame
    void Update()
    {
        if (player1)
        {
            if ((GetComponent<Cell>() && !GetComponent<Cell>().alive) || Input.GetKeyDown(KeyCode.R))
            {

                GridManagement.ResetScene();
            }
            if (Input.GetAxis("Scroll") < 0)
            {
                camComp.orthographicSize = Mathf.Clamp(camComp.orthographicSize + zoomSpeed, minZoom, maxZoom);
            }
            else if (Input.GetAxis("Scroll") > 0)
            {
                camComp.orthographicSize = Mathf.Clamp(camComp.orthographicSize - zoomSpeed, minZoom, maxZoom);

            }
        }
        if (player1 && !moving && Input.GetButtonDown("Fire"))
        {
            foreach (Cell cell in Cell.allCells)
            {
                cell.UpdateNeighbourCounts();

            }

            //get the counts all and then actually tick them all
            foreach (Cell cell in Cell.allCells)
            {
                cell.Tick();
            }

            if (tag == "Pushable" && GridManagement.playerDead)
            {
                //if you died from the tick, reset
                GridManagement.ResetScene();
            }
        }
        else
        {
            if (Input.GetAxis(hInput) < -inputThresh)
            {
                dir = -transform.right;
            }
            else if (Input.GetAxis(hInput) > inputThresh)
            {
                dir = transform.right;
            }
            else if (Input.GetAxis(vInput) > inputThresh)
            {
                dir = transform.up;
            }
            else if (Input.GetAxis(vInput) < -inputThresh)
            {
                dir = -transform.up;
            }
            else
            {
                return;
            }
            if (!moving)
            {
                GameObject thingInTheWay = GridManagement.getObjectFromPosition(transform.position + dir);
                if (thingInTheWay)
                {
                    if (canPush)
                    {
                        if (thingInTheWay.tag == "Pushable")
                        {
                            Move(thingInTheWay);
                            //update the grid to account for the push
                            Invoke("UpdateCellCounts", GridMovementGeneral.instance.timeForLerps + .1f);
                        }
                    }
                }

                moving = true;
                Move(gameObject);
                Invoke("ResetMovin", GridMovementGeneral.instance.timeForLerps + .1f);
            }
        }
    }
    void UpdateCellCounts()
    {
        foreach (Cell cell in Cell.allCells)
        {
            cell.UpdateNeighbourCounts();

        }
    }
    void Move(GameObject thing)
    {
        GridMovementGeneral.MovementByType(thing, thing.transform.position + dir, Quaternion.identity, GridMovementGeneral.MovementTypes.Smooth);

    }
    void ResetMovin()
    {
        moving = false;
    }
    private void OnDestroy()
    {

    }
}
